import React from "react";
import { useView } from "../../APIContext/ViewContext";
import "./Toggle.scss";

const ViewToggle = () => {
  const { view, setView } = useView("card");

  return (
    <div className="view-toggle">
      <button
        onClick={() => {
          view === "card" ? setView("table") : setView("card");
        }}
        className={view === "card" ? "card-cls" : "table-cls"}
      >
        Change View
      </button>
    </div>
  );
};

export default ViewToggle;
