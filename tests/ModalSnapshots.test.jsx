import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import Modal from '../src/components/Modal/Modal';

const mockStore = configureStore([]);
const store = mockStore({});

const card = {
  id: 1,
  name: 'Test Product',
  price: 100,
  color: 'blue',
  path: 'path/to/image.jpg',
};

test('Modal snapshot when visible and modalType is delete', () => {
  const { asFragment } = render(
    <Provider store={store}>
      <Modal
        isVisible={true}
        modalType="delete"
        card={card}
        closeModal={jest.fn()}
        dispatch={jest.fn()}
        deleteFromBasket={jest.fn()}
        addProduct={jest.fn()}
      />
    </Provider>
  );
  expect(asFragment()).toMatchSnapshot();
});

test('Modal snapshot when visible and modalType is add', () => {
  const { asFragment } = render(
    <Provider store={store}>
      <Modal
        isVisible={true}
        modalType="add"
        card={card}
        closeModal={jest.fn()}
        dispatch={jest.fn()}
        deleteFromBasket={jest.fn()}
        addProduct={jest.fn()}
      />
    </Provider>
  );
  expect(asFragment()).toMatchSnapshot();
});

test('Modal snapshot when not visible', () => {
  const { asFragment } = render(
    <Provider store={store}>
      <Modal
        isVisible={false}
        modalType="delete"
        card={card}
        closeModal={jest.fn()}
        dispatch={jest.fn()}
        deleteFromBasket={jest.fn()}
        addProduct={jest.fn()}
      />
    </Provider>
  );
  expect(asFragment()).toMatchSnapshot();
});