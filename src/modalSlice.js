import { createSlice } from "@reduxjs/toolkit";

const modalSlice = createSlice({
  name: "modal",
  initialState: {
    modalStatus: "false",
    modalType: "unknown",
    isVisible: null,
  },
  reducers: {
    modalReducer: (state, action) => {
      state.modalStatus = action.payload;
    },
    setModalType: (state, action) => {
      state.modalType = action.payload;
    },
    setIsVisible: (state, action) => {
      state.isVisible = action.payload;
    },
  },
});

export const { modalReducer, setModalType, setIsVisible } = modalSlice.actions;

export default modalSlice.reducer;
