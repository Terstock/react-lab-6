import React from "react";
import { BrowserRouter, useLocation } from "react-router-dom";

export default function Footer() {
  const location = useLocation();
  const footerClass =
    location.pathname === "/basket" || location.pathname === "/favorite"
      ? "footer-down"
      : "footer";

  return (
    <footer className={footerClass}>
      <p>Was designed and created by M.B</p>
    </footer>
  );
}
