import List from "../components/List/List";
import CustomerForm from "../components/Form/CustomerForm";
import { useSelector } from "react-redux";

function Basket() {
  const basketProducts = useSelector((state) => state.products.basket);
  let pageType = "presentIcon";
  return (
    <>
      {basketProducts.length > 0 ? (
        <div className="basket-div">
          <List data={basketProducts} pageType={pageType} />

          <CustomerForm />
        </div>
      ) : (
        <div className="product-div">
          <h1 className="product-text">Please, add some products first!</h1>
        </div>
      )}
    </>
  );
}

export default Basket;
