import List from "../components/List/List";
import { useSelector } from "react-redux";

function Favorite() {
  const favProducts = useSelector((state) => state.products.favorite);
  return (
    <>
      {favProducts.length > 0 ? (
        <List data={favProducts} />
      ) : (
        <div className="product-div">
          <h1 className="product-text">Please, add some products first!</h1>
        </div>
      )}
    </>
  );
}

export default Favorite;
