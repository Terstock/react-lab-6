import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchProducts, setError } from "../productsSlice";
import ProductsList from "../components/Toggle/ProductsList";
import { ViewProvider } from "../APIContext/ViewContext";
import ViewToggle from "../components/Toggle/ViewToggle";

function MainPage() {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products.products);
  const productStatus = useSelector((state) => state.products.status);
  const error = useSelector((state) => state.products.error);
  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  let content;

  if (productStatus === "loading") {
    content = <div>Loading...</div>;
  } else if (productStatus === "succeeded") {
    content = <ProductsList products={products} />;
  } else if (productStatus === "failed") {
    dispatch(setError("The path is incorrect!"));
    content = <div>{error}</div>;
  }

  return (
    <>
      <ViewProvider>
        <ViewToggle />
        <div className="main-page">{content}</div>
      </ViewProvider>
    </>
  );
}

export default MainPage;
