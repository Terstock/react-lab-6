import { configureStore } from "@reduxjs/toolkit";
import productsSlice, {
  addProduct,
  deleteFromBasket,
  setError,
  handleFavorite,
} from "../src/productsSlice";

describe("myReducer", () => {
  let store;

  beforeEach(() => {
    store = configureStore({
      reducer: {
        products: productsSlice,
      },
    });
  });

  it("works with reducers", () => {
    store.dispatch(addProduct({ id: 1 }));
    expect(store.getState().products).toEqual({
      products: [],
      basket: [{ id: 1 }],
      favorite: [],
      error: null,
    });

    store.dispatch(deleteFromBasket({ id: 1 }));
    expect(store.getState().products).toEqual({
      products: [],
      basket: [],
      favorite: [],
      error: null,
    });

    store.dispatch(handleFavorite({ id: 1 }));
    expect(store.getState().products).toEqual({
      products: [],
      basket: [],
      favorite: [{ id: 1 }],
      error: null,
    });

    store.dispatch(handleFavorite({ id: 1 }));
    expect(store.getState().products).toEqual({
      products: [],
      basket: [],
      favorite: [],
      error: null,
    });

    store.dispatch(setError("error"));
    expect(store.getState().products).toEqual({
      products: [],
      basket: [],
      favorite: [],
      error: "error",
    });
  });
});
