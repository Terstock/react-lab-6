import { render, screen } from "@testing-library/react";
import Button from "../src/components/Button/Button";
import Card from "../src/components/Card/Card";
import { useSelector } from "react-redux";

test("test button", () => {
  render(<Button>Buy Product</Button>);
  const btn = screen.getByText("Buy Product");
  expect(btn).toBeInTheDocument();
});

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

test("test card info", () => {
  const mockCard = {
    id: 2,
    name: "The Division",
    price: 32,
    path: "./photos/game-2.jpg",
    color: "blue",
  };
  const modalProps = [];
  useSelector.mockReturnValue(modalProps);
  render(<Card card={mockCard}></Card>);
  const cardBuyBtn = screen.getByText("Buy Product");
  expect(cardBuyBtn).toBeInTheDocument();

  const favBtn = screen.getByAltText("favorite-image");
  expect(favBtn).toBeInTheDocument();

  const cardColor = screen.getByText("Game disk color: blue");
  expect(cardColor).toBeInTheDocument();
});
