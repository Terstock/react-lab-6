import { ErrorMessage, Field } from "formik";
import React from "react";

function Input({ type, name, id, placeholder, errors, touched }) {
  const inputClassName =
    errors[name] && touched[name]
      ? "input-error"
      : touched[name]
      ? "correct-input"
      : "usual-input";

  return (
    <div className="pageDiv">
      <label htmlFor={id}>Your name:</label>
      <Field
        type={type}
        name={name}
        id={id}
        placeholder={placeholder}
        className={inputClassName}
      />
      <ErrorMessage name={name} component="span" className="error" />
    </div>
  );
}

export default Input;
