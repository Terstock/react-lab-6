import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import { Provider } from 'react-redux';
import store from '../src/store';
import Card from '../src/components/Card/Card';

const mockCard = {
  id: 1,
  name: 'Тестовий продукт',
  price: 120.0,
  path: './books/kobzar.jfif',
  color: 'blue',
  vendorCode: '100001',
};

const renderWithProvider = (children) => {
  return render(<Provider store={store}>{children}</Provider>);
};

describe('Card component', () => {
  test('renders delete button and triggers delete modal', () => {
    renderWithProvider(<Card card={mockCard} pageType="presentIcon" />);
    
    const deleteButton = screen.getByTitle('Delete Product');
    expect(deleteButton).toBeInTheDocument();

    fireEvent.click(deleteButton);

    const modalTitle = screen.getByText('Product Delete!');
    expect(modalTitle).toBeInTheDocument();

    const cancelButton = screen.getByText('NO, CANCEL');
    expect(cancelButton).toBeInTheDocument();

    const deleteConfirmButton = screen.getByText('YES, DELETE');
    expect(deleteConfirmButton).toBeInTheDocument();
  });

  test('renders add button and triggers add modal', () => {
    renderWithProvider(<Card card={mockCard} pageType="otherPageType" />);

    // Перевірка наявності кнопки додавання
    const addButton = screen.getByText('Buy Product');
    expect(addButton).toBeInTheDocument();

    // Виклик модального вікна
    fireEvent.click(addButton);

    // Перевірка наявності модального вікна з кнопками
    const modalTitle = screen.getByText(`Add Product ${mockCard.name}`);
    expect(modalTitle).toBeInTheDocument();

    const addToBasketButton = screen.getByText('ADD TO BASKET');
    expect(addToBasketButton).toBeInTheDocument();
  });
});