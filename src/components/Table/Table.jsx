import "./Table.scss";
import { useDispatch, useSelector } from "react-redux";
import { addProduct, handleFavorite } from "../../productsSlice";
import { setModalType, setIsVisible, modalReducer } from "../../modalSlice";
import falseFavorite from "../assets/favorite-false.svg";
import trueFavorite from "../assets/favorite-true.svg";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import ModalWrapper from "../Modal/ModalWrapper";
import ModalHeader from "../Modal/ModalHeader";
import ModalClose from "../Modal/ModalClose";
import ModalBody from "../Modal/ModalBody";
import ModalFooter from "../Modal/ModalFooter";
import ModalImage from "../Modal/ModalImage";
import ModalText from "../Modal/ModalText";

function Table({ data }) {
  const dispatch = useDispatch();
  const basketProducts = useSelector((state) => state.products.basket);
  const favProducts = useSelector((state) => state.products.favorite);
  const isVisible = useSelector((state) => state.modal.isVisible);
  const modalType = useSelector((state) => state.modal.modalType);

  function openModal(type, id) {
    dispatch(setModalType(type));
    dispatch(setIsVisible(id));
    dispatch(modalReducer("true"));
  }

  function closeModal() {
    dispatch(setModalType("unknown"));
    dispatch(setIsVisible(null));
    dispatch(modalReducer("false"));
  }

  function handleWrapperClick(e) {
    e.stopPropagation();
  }

  return (
    <>
      <table className="table">
        <thead>
          <tr>
            <th>Image</th>
            <th>Name</th>
            <th>Color</th>
            <th>Price</th>
            <th>ID</th>
            <th>Add to Favorite</th>
            <th>Add to Basket</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item, index) => {
            const isFavoritesAlready = favProducts.some(
              (product) => product.id === item.id
            );
            const isInBasket = basketProducts.some(
              (product) => product.id === item.id
            );

            return (
              <tr key={index}>
                <td>
                  <img className="table-img" src={item.path} alt="img" />
                </td>
                <td className="name-td">{item.name}</td>
                <td>{item.color}</td>
                <td className="table-price">{item.price}$</td>
                <td>№{item.id}</td>
                <td className="favorite-td">
                  <button
                    onClick={() => dispatch(handleFavorite(item))}
                    className="favorite-button"
                  >
                    <img
                      className="favorite-img"
                      src={isFavoritesAlready ? trueFavorite : falseFavorite}
                      alt="favorite"
                    />
                  </button>
                </td>
                <td className="basket-td">
                  <Button
                    type="button"
                    onClick={() => openModal("add", item.id)}
                    className="new__button new__button--white"
                    disabled={isInBasket}
                  >
                    {isInBasket ? "Product is in Basket" : "Buy Product"}
                  </Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      {data.map((item) =>
        isVisible === item.id ? (
          <ModalWrapper
            className="modal__wrapper"
            onClick={closeModal}
            key={item.id}
          >
            <Modal className="modal" onClick={handleWrapperClick}>
              <ModalHeader className="modal__header">
                <ModalClose className="modal__close-btn" onClick={closeModal} />
              </ModalHeader>
              <ModalBody className="modal__body">
                {modalType === "delete" ? (
                  <>
                    <ModalImage
                      className="modal__body-rectangle"
                      image={item.path}
                    />
                    <ModalText className="modal__body-title">
                      Product Delete!
                    </ModalText>
                    <ModalText className="modal__body-text">
                      By clicking the “Yes, Delete” button, {item.name} will be
                      deleted.
                    </ModalText>
                  </>
                ) : (
                  <>
                    <ModalText className="modal__body-title">
                      Add Product {item.name}
                    </ModalText>
                    <ModalText className="modal__body-text">
                      Description for {item.name}
                    </ModalText>
                  </>
                )}
              </ModalBody>
              {modalType === "delete" ? (
                <ModalFooter
                  firstText="NO, CANCEL"
                  secondaryText="YES, DELETE"
                  firstClick={closeModal}
                  secondaryClick={() => {
                    dispatch(deleteFromBasket(item)), closeModal();
                  }}
                />
              ) : (
                <ModalFooter
                  firstText="ADD TO BASKET"
                  firstClick={() => {
                    dispatch(addProduct(item)), closeModal();
                  }}
                />
              )}
            </Modal>
          </ModalWrapper>
        ) : null
      )}
    </>
  );
}

export default Table;
