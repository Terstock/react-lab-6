import { useView } from "../../APIContext/ViewContext";
import List from "../List/List"; // ваш компонент List
import Table from "../Table/Table"; // ваш компонент Table

const ProductsList = ({ products }) => {
  const { view } = useView();

  return (
    <div className="products-list">
      {view === "card" ? <List data={products} /> : <Table data={products} />}
    </div>
  );
};

export default ProductsList;
