import React from 'react';
import { render } from '@testing-library/react';
import Card from '../src/components/Card/Card';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import Button from '../src/components/Button/Button';

const mockStore = configureStore([]);
const store = mockStore({
  modal: { isVisible: null, modalType: 'unknown' },
  products: { basket: [], favorite: [] },
});

test('Card snapshot', () => {
  const card = {
    id: 1,
    name: 'Test Product',
    price: 100,
    color: 'blue',
    path: 'path/to/image.jpg',
  };

  const { asFragment } = render(
    <Provider store={store}>
      <Card
        card={card}
        pageType="presentIcon"
        isFavoritesAlready={false}
        openModal={jest.fn()}
        dispatch={jest.fn()}
        handleFavorite={jest.fn()}
        isInBasket={false}
      />
    </Provider>
  );
  expect(asFragment()).toMatchSnapshot();
});

test('Button snapshot inside Card', () => {
  const card = {
    id: 1,
    name: 'Test Product',
    price: 100,
    color: 'blue',
    path: 'path/to/image.jpg',
  };

  const { getByText } = render(
    <Provider store={store}>
      <Card
        card={card}
        pageType="presentIcon"
        isFavoritesAlready={false}
        openModal={jest.fn()}
        dispatch={jest.fn()}
        handleFavorite={jest.fn()}
        isInBasket={false}
      />
    </Provider>
  );

  const button = getByText('Buy Product');
  expect(button).toMatchSnapshot();

  
});