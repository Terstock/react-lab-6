import { Form, Field, ErrorMessage, Formik } from "formik";
import "./CustomerForm.css";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { basketReducer } from "../../productsSlice";
import "yup-phone";
import Input from "./Input";

const SignScheme = Yup.object().shape({
  name: Yup.string()
    .required("Name is required!")
    .min(5, "Name is too short!")
    .max(10, "Name is too long!"),
  surname: Yup.string()
    .required("Surname is required!")
    .min(5, "Surname is too short!")
    .max(15, "Surname is too long!"),
  dateBirth: Yup.date()
    .max(new Date(), "Date of birth must be in the past!")
    .required("Date of birth is required!"),
  adress: Yup.string().required("Adress is required!"),
  phoneNumber: Yup.string()
    .matches(/^\+?[0-9]+\d{9}$/, {
      message: "Please enter valid number.",
      excludeEmptyString: false,
    })
    .required("Phone number is required!"),
});

const initialValues = {
  name: "",
  surname: "",
  dateBirth: "",
  adress: "",
  phoneNumber: "",
};

function SignInForm() {
  const dispatch = useDispatch();
  const basket = useSelector((state) => state.products.basket);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={SignScheme}
      onSubmit={(values, { resetForm }) => {
        if (basket.length >= 1) {
          console.log("Customer items:", basket);
          console.log("Customer info:", values);
          dispatch(basketReducer([]));
        }
        resetForm();
      }}
    >
      {({ errors, touched, isValid, dirty }) => (
        <div className="form">
          <h1 className="form-text">
            Enter information to complete the order:
          </h1>
          <Form className="pageForm">
            <Input
              type="text"
              name="name"
              id="id"
              placeholder="Please, enter Name:"
              errors={errors}
              touched={touched}
            />
            <Input
              type="text"
              name="surname"
              id="surname"
              placeholder="Please, enter Surname:"
              errors={errors}
              touched={touched}
            />
            <Input
              type="date"
              name="dateBirth"
              id="dateBirth"
              placeholder="Please, enter your date og birth:"
              errors={errors}
              touched={touched}
            />
            <Input
              type="text"
              name="adress"
              id="adress"
              placeholder="Please, enter adress:"
              errors={errors}
              touched={touched}
            />
            <Input
              type="text"
              name="phoneNumber"
              id="phoneNumber"
              placeholder="Please, enter phone number:"
              errors={errors}
              touched={touched}
            />
            <button
              type="submit"
              className={!(dirty && isValid) ? "disabled-btn" : "active-btn"}
              disabled={!(dirty && isValid)}
            >
              Checkout
            </button>
          </Form>
        </div>
      )}
    </Formik>
  );
}

export default SignInForm;
