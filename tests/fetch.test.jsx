import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import { Provider } from 'react-redux'
import store from '../src/store'
import MainPage from "../src/pages/MainPage"

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve([
        {
          name: "Grand Theft Auto",
          price: 52,
          path: "./photos/game-1.jpg",
          id: 340912,
          color: "blue"
        },
      ]),
  })
)

const renderWithProvider = (children) => {
  return render(<Provider store={store}>{children}</Provider>)
}

describe('fetch and render', () => {
  test('should fetch and render books', async () => {
    renderWithProvider(<MainPage />)
    const userElement = await screen.findByText('Grand Theft Auto')
    expect(userElement).toBeInTheDocument()
  })

  test('should fetch and render button', async () => {
    renderWithProvider(<MainPage />)
    const buttonElement = await screen.findByText('Buy Product')
    expect(buttonElement).toBeInTheDocument()
  })
})
